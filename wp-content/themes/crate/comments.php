<?php
    if(post_password_required()) {
    	return;
    }
?>
<div id="comments" class="comments">
	<?php if(have_comments()) : ?>
	<h5 class="standard-title"><?php comments_number( __('No Comments', 'crate'), __('One Comment','crate'), __('% Comments','crate') ); ?></h5>

	<?php get_template_part("includes/walkers/walker-comments"); ?>

	<?php
		wp_list_comments( array(
			'short_ping' => true,
			'avatar_size'=> 34,
			'max_depth'  => 1,
			'walker'     => new comment_walker
		) );
	?>

	<?php endif; ?>

	<?php if (comments_open() ) : ?>
		<h5 class="standard-title"><?php echo esc_attr__("Write a comment","crate"); ?></h5>
	<?php endif; ?>

	<?php
		$fields =  array(
			'<div class="form">',

    			'author' =>"<div class='form-input'><label>" . __("Name", "crate") . "</label><input type='text' id='author' name='author' ></div>",

    			'email' =>"<div class='form-input'><label>" . __("Email", "crate") . "</label><input type='email' id='email' name='email'></div>",

    			'website' =>"<div class='form-input'><label>" . __("Website", "crate") . "</label><input type='text' id='website' name='website'></div>",

			'</div>'
		);
		$args = array(
				'fields' => apply_filters( 'comment_form_default_fields', $fields ),
				'comment_field' => "<div class='form'><label>". esc_attr__("Your Comment","crate") ."</label><textarea id='comment' name='comment'></textarea></div>",
				'title_reply' => '',
				'title_reply_to' => '',
				'label_submit' => __('Post Comment',"crate")
			);
	?>
	<div class="comment-form">
		<?php comment_form($args); ?>
	</div>
</div><!-- #comments -->

<div class="display-none">
	<?php paginate_comments_links(); ?>
</div>
