<?php
    /* Template Name: Contact */
    get_header();

    global $post;
    $content = $post->post_content;
    if(!empty($content)) :
?>
    <div class="page-title">
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-lg-8 col-md-8 col-sm-12">
                <?php echo wpautop($content); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="contact">
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-lg-8 col-md-8 col-sm-12">
                <?php
                    $crate_contact_form = get_field("neuron_contact_form");
                    if($crate_contact_form){
                        echo do_shortcode('[acf field="neuron_contact_form"]');
                    }
                ?>
            </div>
        </div>
    </div>
<?php
    get_footer();
?>
