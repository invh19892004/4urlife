<?php
    /* Template Name: Blog */
    get_header();

    global $post;
    $content = $post->post_content;
    if(!empty($content)) :
?>
    <div class="page-title">
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-lg-8 col-md-8 col-sm-12">
                <?php echo wpautop($content); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
    <div class="blog">
        <?php
            // Blog Columns
            $crate_blog_columns = get_field("neuron_blog_columns");
            if(!$crate_blog_columns){
                $crate_blog_columns = 1;
            }
            if($crate_blog_columns == '1'){
                $crate_blog_columns = get_theme_mod("neuron_blog_columns");
                $crate_blog_columns_mod = $crate_blog_columns;
            }
            else {
                $crate_blog_columns_mod = $crate_blog_columns - 1;
            }

            // Blog Style
            $crate_blog_style = get_field("neuron_blog_style");
            $crate_blog_content = "col-lg-9 col-md-9 col-sm-9 col-xs-12";
            $crate_blog_sidebar = "col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar";
            $crate_blog_post = "post";

            if(!$crate_blog_style){
                $crate_blog_style = 1;
            }
            if($crate_blog_style == '1'){
                $crate_blog_style = get_theme_mod("neuron_blog_style");
                $crate_blog_style_mod = $crate_blog_style;
            }
            else {
                $crate_blog_style_mod = $crate_blog_style - 1;
            }
            switch($crate_blog_style_mod){
                case '1':
                case 1:
                    $crate_blog_content = "col-lg-9 col-md-9 col-sm-9 col-xs-12";
                    $crate_blog_sidebar = "col-lg-3 col-md-3 col-sm-3 col-xs-12 sidebar";
                    break;
                case '2':
                case 2:
                    $crate_blog_content = "grid-blog masonry";
                    $crate_blog_sidebar = "display-none";
                    switch($crate_blog_columns_mod){
                        case '1':
                        case 1:
                            $crate_blog_content = "col-lg-offset-1 col-md-offset-1 col-lg-10 col-md-10 col-sm-12";
                            break;
                        case '2':
                        case 2:
                            $crate_blog_post = "col-md-6 col-sm-6 col-xs-12 selector post";
                            break;
                        case '3':
                        case 3:
                            $crate_blog_post = "col-md-4 col-sm-6 col-xs-12 selector post";
                            break;
                        case '4':
                        case 4:
                            $crate_blog_post = "col-md-3 col-sm-6 col-xs-12 selector post";
                            break;
                        }
                    break;
            }
        ?>
        <div class="row">
            <div class="<?php echo esc_attr($crate_blog_content); ?>">
                <?php
                    // Posts per Page
                    $crate_blog_ppp_mod = get_theme_mod("neuron_blog_ppp");
                    $crate_blog_posts_per_page = get_field("neuron_blog_ppp");
                    if($crate_blog_posts_per_page){
                        $crate_blog_ppp = $crate_blog_posts_per_page;
                    }
                    else {
                        if($crate_blog_ppp_mod){
                            $crate_blog_ppp = $crate_blog_ppp_mod;
                        }
                        else {
                            $crate_blog_ppp = 9;
                        }
                    }

                    // Blog Category
                    $crate_blog_category = get_field("neuron_blog_category");
                    if($crate_blog_category){
                        $crate_blog_cat = "IN";
                    }
                    else {
                        $crate_blog_cat = "NOT IN";
                    }

                    // Paged
                    if(get_query_var('paged')){
                        $paged = get_query_var('paged');
                    }
                    elseif(get_query_var('page')){
                        $paged = get_query_var('page');
                    }
                    else {
                        $paged = 1;
                    }

                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => $crate_blog_ppp,
                        'paged' => $paged,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'category',
                                'field' => 'term_id',
                                'terms' => $crate_blog_category,
                                'operator' => $crate_blog_cat,
                            )
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
                ?>
                    <div id="id-<?php echo esc_attr(get_the_id()); ?>" <?php post_class($crate_blog_post); ?>>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <?php
                                    if(has_post_thumbnail()){
                                        the_post_thumbnail();
                                    }
                                ?>
                            </a>
                        </div>
                        <div class="post-meta">
                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <ul class="underline">
                                <li class="date"><?php the_time('F j, Y'); ?></li>
                                <li class="category"><?php the_category(' '); ?></li>
                            </ul>
                        </div>
                        <div class="post-info">
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="button-holder">
                            <a href="<?php the_permalink(); ?>" class="button"><?php echo esc_attr__("Read More", "crate"); ?></a>
                        </div>
                    </div>
                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
            <?php if($crate_blog_style_mod != '2') : ?>
                <div class="<?php echo esc_attr($crate_blog_sidebar); ?>">
                    <?php get_sidebar(); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="<?php echo esc_attr($crate_blog_style_mod == '2' || $crate_blog_style_mod == 2) ? "col-lg-offset-1 col-md-offset-1 col-lg-10 col-md-10 col-sm-12" : "col-lg-12"; ?>">
                <?php neuron_pagination($query->max_num_pages, "neuron-pagination", 999); ?>
            </div>
        </div>
    </div>
<?php
    get_footer();
?>
