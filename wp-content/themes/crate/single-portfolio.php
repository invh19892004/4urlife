<?php
    get_header();

    if(have_posts()) : while(have_posts()) : the_post();

    // Content Position
    $crate_portfolio_item_content_position = get_field("neuron_portfolio_item_content_position");
    $crate_portfolio_item_content = "col-lg-12";
    $crate_portfolio_item_images = "col-lg-12 portfolio-holder";
    $crate_portfolio_item_row = "row";

    if(!$crate_portfolio_item_content_position){
        $crate_portfolio_item_content_position = 1;
    }
    if($crate_portfolio_item_content_position == '1'){
        $crate_portfolio_item_content_position = get_theme_mod("neuron_portfolio_item_content_position");
        $crate_portfolio_item_content_position_mod = $crate_portfolio_item_content_position;
    }
    else {
        $crate_portfolio_item_content_position_mod = $crate_portfolio_item_content_position - 1;
    }
    switch($crate_portfolio_item_content_position_mod){
        case '2':
        case 2:
            $crate_portfolio_item_content = "col-lg-12 order-content";
            $crate_portfolio_item_images = "col-lg-12 portfolio-holder";
            $crate_portfolio_item_row = "row order-single";
        break;
        case '3':
        case 3:
            $crate_portfolio_item_content = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
            $crate_portfolio_item_images = "col-lg-6 col-md-6 col-sm-6 col-xs-12 portfolio-holder";
        break;
        case '4':
        case 4:
            $crate_portfolio_item_content = "col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right";
            $crate_portfolio_item_images = "col-lg-6 col-md-6 col-sm-6 col-xs-12 portfolio-holder";
        break;
    }
?>
<div class="<?php echo esc_attr($crate_portfolio_item_row) ?>">
    <div class="<?php echo esc_attr($crate_portfolio_item_content); ?>">
        <div class="project-title">
            <h5><?php the_title(); ?></h5>
        </div>
        <div class="project-description">
            <?php the_content(); ?>
        </div>
    </div>
    <div class="<?php echo esc_attr($crate_portfolio_item_images); ?>">
        <?php
            // Fancybox Url
            $crate_portfolio_item_gallery_fancybox = get_field("neuron_portfolio_item_gallery_fancybox");
            $crate_portfolio_item_fancybox_mod = get_theme_mod("neuron_portfolio_item_fancybox");
            $crate_portfolio_item_holder = "portfolio alternative row";
            $crate_portfolio_item_url = "#";

            if($crate_portfolio_item_gallery_fancybox || $crate_portfolio_item_fancybox_mod == '1'){
                $crate_portfolio_item_holder .= " gallery-active";
            }

            // Gallery Columns
            $crate_portfolio_item_gallery_columns = get_field("neuron_portfolio_item_gallery_columns");
            $crate_portfolio_item_gallery_item = "col-lg-12 selector";
            if(!$crate_portfolio_item_gallery_columns){
                $crate_portfolio_item_gallery_columns = 1;
            }
            if($crate_portfolio_item_gallery_columns == '1'){
                $crate_portfolio_item_gallery_columns = get_theme_mod("neuron_portfolio_item_gallery_columns");
                $crate_portfolio_columns_mod = $crate_portfolio_item_gallery_columns;
            }
            else {
                $crate_portfolio_columns_mod = $crate_portfolio_item_gallery_columns - 1;
            }
            switch($crate_portfolio_columns_mod){
                case '1':
                case 1:
                    $crate_portfolio_item_gallery_item = "col-lg-12 selector";
                    break;
                case '2':
                case 2:
                    $crate_portfolio_item_gallery_item = "col-lg-6 col-md-6 col-sm-6 col-xs-12 selector";
                    break;
                case '3':
                case 3:
                    $crate_portfolio_item_gallery_item = "col-lg-4 col-md-4 col-sm-6 col-xs-12 selector";
                    break;
                case '4':
                case 4:
                    $crate_portfolio_item_gallery_item = "col-lg-3 col-md-3 col-sm-6 col-xs-12 selector";
                    break;
            }

            // Embed Video
            $crate_portfolio_item_embed = get_field("neuron_portfolio_item_embed");
            $crate_portfolio_item_embed_video_position = get_field("neuron_portfolio_item_embed_video_position");

            if($crate_portfolio_item_embed && $crate_portfolio_item_embed_video_position != '2'){
                if(have_rows("neuron_portfolio_item_embed_videos")){
                    while(have_rows("neuron_portfolio_item_embed_videos")){
                        the_row();
                        $crate_portfolio_item_embed_video = get_sub_field("neuron_portfolio_item_embed_video");
                        echo "<div class='embed-container'>" . $crate_portfolio_item_embed_video . "</div>";
                    }
                }
            }
        ?>
        <div class="<?php echo esc_attr($crate_portfolio_item_holder); ?>">
            <?php
                if(have_rows("neuron_portfolio_item_gallery")) : while(have_rows("neuron_portfolio_item_gallery")) : the_row();

                    $crate_portfolio_item_gallery_description = get_sub_field("neuron_portfolio_item_gallery_description");
                    $crate_portfolio_item_gallery_image = get_sub_field("neuron_portfolio_item_gallery_image");

                    if($crate_portfolio_item_gallery_fancybox || $crate_portfolio_item_fancybox_mod == '1'){
                        $crate_portfolio_item_url = $crate_portfolio_item_gallery_image;
                    }
            ?>
                <div class="<?php echo esc_attr($crate_portfolio_item_gallery_item); ?>">
                    <a href="<?php echo esc_attr($crate_portfolio_item_url); ?>" title="<?php echo esc_attr($crate_portfolio_item_gallery_description); ?>">
                        <div class="item-holder">
                            <div class="item">
                                <div class="overlay">
                                    <div class="inner-overlay">
                                        <?php
                                            if($crate_portfolio_item_gallery_description){
                                                echo "<h3>" . $crate_portfolio_item_gallery_description . "</h3>";
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <img src="<?php echo esc_attr($crate_portfolio_item_gallery_image); ?>" alt="">
                        </div>
                    </a>
                </div>
            <?php endwhile; endif; ?>
        </div>
        <?php
            if($crate_portfolio_item_embed && $crate_portfolio_item_embed_video_position == '2'){
                if(have_rows("neuron_portfolio_item_embed_videos")){
                    while(have_rows("neuron_portfolio_item_embed_videos")){
                        the_row();
                        $crate_portfolio_item_embed_video = get_sub_field("neuron_portfolio_item_embed_video");
                        echo "<div class='embed-container'>" . $crate_portfolio_item_embed_video . "</div>";
                    }
                }
            }
        ?>
    </div>
</div>
<?php
    $crate_portfolio_item_navigation = get_theme_mod("neuron_portfolio_item_navigation");
    $crate_portfolio_item_backtoportfolio = get_theme_mod("neuron_portfolio_item_backtoportfolio");
    if($crate_portfolio_item_navigation == null){
        $crate_portfolio_item_navigation = '1';
    }
    if($crate_portfolio_item_navigation == '1' || $crate_portfolio_item_navigation == 1) :
?>
    <div class="single-navigation">
        <?php
            previous_post_link('%link',"<i class='fa fa-angle-left'></i>", false);
            if($crate_portfolio_item_backtoportfolio) :
        ?>
            <div class="back">
                <a href="<?php echo esc_attr(get_page_link($crate_portfolio_item_backtoportfolio)); ?>">
                    <div class="icon-back-to-portfolio"></div>
                </a>
            </div>
        <?php
            endif;
            next_post_link('%link',"<i class='fa fa-angle-right'></i>", false);
        ?>
    </div>
<?php endif; ?>
<?php
    endwhile; endif;

    get_footer();
?>
