<?php

// Language Setup
add_action('after_setup_theme', 'neuron_language_setup');
function neuron_language_setup(){
	load_theme_textdomain('crate', get_template_directory() . '/language');
}

// Set max content width
if(!isset($content_width)){
	$content_width = 1170;
}

// Crate Setup
add_action( 'after_setup_theme', 'neuron_setup' );
function neuron_setup() {
	// Add theme support
	add_theme_support('menus');
	add_theme_support('post-thumbnails');
	add_theme_support('automatic-feed-links');
	add_theme_support('title-tag');

	// Include custom files
	require_once( get_template_directory() . '/includes/tgm.php');
	include_once( get_template_directory() . '/includes/custom-fields.php');
	define( 'ACF_LITE', true );

	// Add stylesheets and scripts
	add_action('wp_enqueue_scripts', 'neuron_add_external_css');
	add_action('wp_enqueue_scripts', 'neuron_add_external_js');
	add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

	// Register Menus
	register_nav_menus(
		array(
			'main-menu' => __('Main Menu', 'crate'),
		)
	);

	// TGMPA
	add_action( 'tgmpa_register', 'neuron_plugins' );
	function neuron_plugins() {
		$plugins = array(
	        array(
	            'name'      => esc_attr__('Contact Form 7', 'crate'),
	            'slug'      => 'contact-form-7',
	            'required'  => false,
	        ),
			array(
				'name'      => esc_attr__('Advanced Custom Fields Font Awesome','crate'),
				'slug'      => 'advanced-custom-fields-font-awesome',
				'required'  => false,
			),
			array(
	        	'name'      => esc_attr__( 'Advanced Custom Fields', 'crate'),
	            'slug'      => 'advanced-custom-fields',
	            'required'  => true,
	        ),
			array(
	        	'name'      => esc_attr__( 'ACF Repeater', 'crate'),
	            'slug'      => 'acf-repeater',
				'source'    => get_template_directory() . '/includes/plugins/acf-repeater.zip',
	            'required'  => true,
	        ),
			array(
	        	'name'      => esc_attr__( 'ACF Flexible Content', 'crate'),
	            'slug'      => 'acf-flexible-content',
				'source'    => get_template_directory() . '/includes/plugins/acf-flexible-content.zip',
	            'required'  => true,
	        ),
			array(
	        	'name'      => esc_attr__( 'Portfolio Post Type', 'crate'),
	            'slug'      => 'portfolio-post-type',
	            'required'  => true,
	        ),
			array(
	        	'name'      => esc_attr__( 'One Click Demo Importer', 'crate'),
	            'slug'      => 'one-click-demo-import',
	            'required'  => true,
	        ),
			array(
	        	'name'      => esc_attr__( 'WP Forms', 'crate'),
	            'slug'      => 'wpforms-lite',
	            'required'  => false,
	        ),
	    );
		$config = array(
			'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
			'default_path' => '',                      // Default absolute path to bundled plugins.
			'menu'         => 'tgmpa-install-plugins', // Menu slug.
			'parent_slug'  => 'themes.php',            // Parent menu slug.
			'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
			'has_notices'  => true,                    // Show admin notices or not.
			'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
			'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
			'is_automatic' => false,                   // Automatically activate plugins after installation or not.
			'message'      => '',                      // Message to output right before the plugins table.
		);
	    tgmpa( $plugins, $config );
	}
}

// Register Fonts
function neuron_fonts_url() {
	$font_url = '';
	if ( 'off' !== _x( 'on', 'Google font: on or off', 'crate')){
		$font_url = add_query_arg( 'family', urlencode( 'Hind:400,600|Poppins:400,500' ), "//fonts.googleapis.com/css");
	}
	return $font_url;
}

// External CSS
function neuron_add_external_css() {
	// Register
	wp_register_style('neuron-style', get_stylesheet_uri());
	wp_register_style('bootstrap', get_template_directory_uri().'/assets/css/bootstrap.min.css', false, "1.0");
	wp_register_style('font-awesome', get_template_directory_uri().'/assets/css/font-awesome.min.css', false, "1.0");
	wp_register_style('magnific-popup', get_template_directory_uri().'/assets/css/magnific-popup.min.css', false, "1.0");
	wp_register_style('neuron-main-style', get_template_directory_uri().'/assets/css/style.css', false, "1.0");

	// Enqueue
	wp_enqueue_style('neuron-style');
	wp_enqueue_style('bootstrap');
	wp_enqueue_style('font-awesome');
	wp_enqueue_style('magnific-popup');
	wp_enqueue_style('neuron-main-style');
	wp_enqueue_style('neuron-fonts', neuron_fonts_url(), array(), '1.0' );
}

// External Javascript
function neuron_add_external_js() {
	if(!is_admin()) {
		// Register
		wp_register_script('bootstrap', get_template_directory_uri().'/assets/js/bootstrap.min.js', array('jquery'), '1.0', TRUE);
		wp_register_script('isotope', get_template_directory_uri().'/assets/js/isotope.pkgd.min.js', array('jquery'), '1.0', TRUE);
		wp_register_script('magnific-popup', get_template_directory_uri().'/assets/js/jquery.magnific-popup.min.js', array('jquery'), '1.0', TRUE);
		wp_register_script('main', get_template_directory_uri().'/assets/js/main.js', array('jquery'), '1.0', TRUE);

		wp_enqueue_script('bootstrap');
		wp_enqueue_script('isotope');
		wp_enqueue_script('magnific-popup');
		wp_enqueue_script('main');
		if (is_singular()) wp_enqueue_script( "comment-reply" );
	}

}

// Favicon
if (!function_exists('wp_site_icon') || !has_site_icon()){
	function neuron_favicon() {
		$crate_favicon = get_theme_mod('neuron_favicon');
		if($crate_favicon) {
			echo "<link rel='shortcut icon' href=".esc_url($crate_favicon)." />";
		}
	}
	add_action('wp_head', 'neuron_favicon');
}


// Sidebar
add_action( 'widgets_init', 'neuron_widgets_init' );
function neuron_widgets_init() {
    register_sidebar(
    	array(
			'name' => __("Main Sidebar","crate"),
			'description' => __("Widgets on this sidebar are displayed in Blog Page.", "crate"),
			'id' => 'sidebar-1',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h6 class="widgettitle">',
			'after_title'   => '</h6>',
    	)
	);
}

// Pagination
function neuron_pagination($pages = '', $class = '', $range = 4) {
	 $showitems = ($range * 2)+1;
	 global $paged;
	 if(empty($paged)) $paged = 1;
	 if($pages == ''){
		 global $wp_query;
		 $pages = $wp_query->max_num_pages;
		 if(!$pages) {
		 	$pages = 1;
		 }
	 }

	 $pagination_class = $class;

	 if(1 != $pages){
 		 echo "<ul class=\"" . esc_attr($pagination_class)  . "\">";
 		 if($paged > 1){
 			 echo "<span class='prev'><a href='".get_pagenum_link($paged - 1)."'><i class='fa fa-angle-left'></i></a></span>";
 		 }
 		 for ($i=1; $i <= $pages; $i++){
 			 if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
 			 	echo ($paged == $i)? "<li class=\"active\"><a>".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a></li>";
 			 }
 		 }
 		 $pages_float = intval($pages);
 		 if($paged >= 1 && $paged != $pages_float){
 			 echo "<span class='next'><a href='".get_pagenum_link($paged + 1)."'><i class='fa fa-angle-right'></i></a></span>";
 		 }
 		 echo "</ul>\n";
 	 }
}

// ACF if not activated
if(!function_exists('get_field') && ! is_admin()){
	function get_field($field_id, $post_id = null){
		return null;
	}
}

if(!function_exists('get_sub_field') && ! is_admin()){
	function get_sub_field($field_id, $post_id = null){
		return null;
	}
}

// Content with custom length
function neuron_custom_content($limit) {
	$crate_custom_content = explode(' ', get_the_content(), $limit);
	if(count($crate_custom_content) >= $limit) {
	array_pop($crate_custom_content);
		$crate_custom_content = implode(" ",$crate_custom_content).'...';
	}
	else {
		$crate_custom_content = implode(" ",$crate_custom_content);
	}
	$crate_custom_content = preg_replace('`\[[^\]]*\]`','',$crate_custom_content);
	return $crate_custom_content;
}

// Customizer Options
function neuron_customize_options($wp_customize){
	// General Settings
	$wp_customize->add_setting('neuron_favicon', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_responsivity', array('transport' => 'refresh', 'default' => 'enable',array('sanitize_callback' => '__return_false',)));

	// Header Settings
	$wp_customize->add_setting('neuron_header_logo', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_header_logo_text', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_header_logo_width', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_header_logo_height', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));

	// Footer Settings
	$wp_customize->add_setting('neuron_footer_alignment', array('transport' => 'refresh','default' => 'center',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_footer_copyright', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));

	// Style Settings
	$wp_customize->add_setting('neuron_body_color', array('default' => '#fff','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_header_color', array('default' => '#303133','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_wrapper_color', array('default' => '#fff','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));

	// Portfolio Settings
	$wp_customize->add_setting('neuron_portfolio_style', array('default' => '1','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_portfolio_fancybox', array('default' => '2','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_portfolio_columns', array('default' => '3','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_portfolio_ppp', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));

	// Portfolio Item Settings
	$wp_customize->add_setting('neuron_portfolio_item_content_position', array('default' => '1','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_portfolio_item_gallery_columns', array('default' => '1','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_portfolio_item_fancybox', array('default' => '2','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_portfolio_item_navigation', array('default' => '1','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_portfolio_item_backtoportfolio', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));

	// Blog Settings
	$wp_customize->add_setting('neuron_blog_style', array('default' => '1','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_blog_columns', array('default' => '1','transport' => 'refresh',array('sanitize_callback' => '__return_false',)));
	$wp_customize->add_setting('neuron_blog_ppp', array('transport' => 'refresh',array('sanitize_callback' => '__return_false',)));

	// Sections
    $wp_customize->add_section('neuron_general_options', array('title' => __('General Options', 'crate'), 'priority' => 3));
    $wp_customize->add_section('neuron_header_options',array('title' => __('Header Options', 'crate'), 'priority' => 4));
	$wp_customize->add_section('neuron_footer_options',array('title' => __('Footer Options', 'crate'), 'priority' => 5));
	$wp_customize->add_section('neuron_style_options',array('title' => __('Style Options', 'crate'), 'priority' => 6));
	$wp_customize->add_section('neuron_portfolio_options',array('title' => __('Portfolio Options', 'crate'), 'priority' => 7));
	$wp_customize->add_section('neuron_portfolio_item_options',array('title' => __('Portfolio Item Options', 'crate'), 'priority' => 8));
	$wp_customize->add_section('neuron_blog_options',array('title' => __('Blog Options', 'crate'), 'priority' => 9));

	// General Controls
	$wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, 'neuron_favicon_control',array(
		'label' => __('Favicon', 'crate'),
		'description' => __('Upload your favicon, 16x16 preferred size in PNG format.','crate'),
		'section' => 'neuron_general_options',
        'settings' => 'neuron_favicon'
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_responsivity_control',array(
		'label' => __('Responsivity', 'crate'),
		'description' => __('Switch on or off the responsivity, if you disable the responsive your website will remain the same on small devices.','crate'),
		'section' => 'neuron_general_options',
        'settings' => 'neuron_responsivity',
		'type' => 'select',
		'choices' => array(
			'enable'   => __('Enable','crate'),
			'disable'   => __('Disable','crate')
		)
	)));

	// Header Controls
	$wp_customize->add_control(new WP_Customize_Upload_Control($wp_customize, 'neuron_header_logo_control',array(
		'label' => __('Logo', 'crate'),
		'description' => __('Upload your logo from gallery, incase you don\'t want to use site title or logo text.','crate'),
		'section' => 'neuron_header_options',
        'settings' => 'neuron_header_logo'
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_header_logo_text_control',array(
		'label' => __('Text Logo', 'crate'),
		'description' => __('Enter the text that will appear as logo.','crate'),
		'section' => 'neuron_header_options',
        'settings' => 'neuron_header_logo_text',
		'type' => 'text'
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_header_logo_width_control',array(
		'label' => __('Logo Width', 'crate'),
		'description' => __('Enter the number to change the logo image width, enter only the number without {px}.','crate'),
		'section' => 'neuron_header_options',
        'settings' => 'neuron_header_logo_width',
		'type' => 'text'
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_header_logo_height_control',array(
		'label' => __('Logo Height', 'crate'),
		'description' => __('Enter the number to change the logo image height, enter only the number without {px}.','crate'),
		'section' => 'neuron_header_options',
        'settings' => 'neuron_header_logo_height',
		'type' => 'text'
	)));

	// Footer Controls
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_footer_alignment_control',array(
		'label' => __('Footer Alignment', 'crate'),
		'description' => __('Select the alignment of content.','crate'),
		'section' => 'neuron_footer_options',
        'settings' => 'neuron_footer_alignment',
		'type' => 'select',
		'choices' => array(
            'left'   => __('Left','crate'),
            'center'   => __('Center','crate'),
            'right'   => __('Right','crate')
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_footer_copyright_control',array(
		'label' => __('Footer Copyright', 'crate'),
		'description' => __('Enter the text that will appear in footer as copyright.','crate'),
		'section' => 'neuron_footer_options',
        'settings' => 'neuron_footer_copyright',
		'type' => 'textarea'
	)));
	// Style Controls
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'neuron_body_color_control',array(
		'label' => __('Body', 'crate'),
		'description' => __('Change the color of body.','crate'),
		'section' => 'neuron_style_options',
        'settings' => 'neuron_body_color'
	)));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'neuron_header_color_control',array(
		'label' => __('Header', 'crate'),
		'description' => __('Change the color of text in header.','crate'),
		'section' => 'neuron_style_options',
		'settings' => 'neuron_header_color'
	)));
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'neuron_wrapper_color_control',array(
		'label' => __('Wrapper', 'crate'),
		'description' => __('Change the color of wrapper holder.','crate'),
		'section' => 'neuron_style_options',
		'settings' => 'neuron_wrapper_color'
	)));
	// Portfolio Controls
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_style_control',array(
		'label' => __('Style', 'crate'),
		'description' => __('Select the style layout of Portfolio.','crate'),
		'section' => 'neuron_portfolio_options',
		'settings' => 'neuron_portfolio_style',
		'type' => 'select',
		'choices' => array(
            '1'   => __('Default','crate'),
            '2'   => __('Alternative','crate'),
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_fancybox_control',array(
		'label' => __('Fancybox', 'crate'),
		'description' => __('Switch on the Gallery, the projects will be displayed in fancybox slider.','crate'),
		'section' => 'neuron_portfolio_options',
		'settings' => 'neuron_portfolio_fancybox',
		'type' => 'select',
		'choices' => array(
            '1'   => __('On','crate'),
            '2'   => __('Off','crate'),
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_columns_control',array(
		'label' => __('Columns', 'crate'),
		'description' => __('Select the columns that you want to show your projects.','crate'),
		'section' => 'neuron_portfolio_options',
		'settings' => 'neuron_portfolio_columns',
		'type' => 'select',
		'choices' => array(
            '1'   => __('Full Width','crate'),
            '2'   => __('2 Columns','crate'),
            '3'   => __('3 Columns','crate'),
            '4'   => __('4 Columns','crate')
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_ppp_control',array(
		'label' => __('Post Per Page', 'crate'),
		'description' => __('Write how many portfolio items you want to display, write -1 if you want to show all the posts.','crate'),
		'section' => 'neuron_portfolio_options',
		'settings' => 'neuron_portfolio_ppp',
		'type' => 'text'
	)));
	// Portfolio Item Options
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_item_content_position_control',array(
		'label' => __('Content Position', 'crate'),
		'description' => __('Select the position of content, default is top.','crate'),
		'section' => 'neuron_portfolio_item_options',
		'settings' => 'neuron_portfolio_item_content_position',
		'type' => 'select',
		'choices' => array(
            '1'   => __('Top','crate'),
            '2'   => __('Bottom','crate'),
            '3'   => __('Left','crate'),
            '4'   => __('Right','crate'),
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_item_gallery_columns_control',array(
		'label' => __('Gallery Columns', 'crate'),
		'description' => __('Select the columns of gallery.','crate'),
		'section' => 'neuron_portfolio_item_options',
		'settings' => 'neuron_portfolio_item_gallery_columns',
		'type' => 'select',
		'choices' => array(
            '1'   => __('Full Width','crate'),
            '2'   => __('2 Columns','crate'),
            '3'   => __('3 Columns','crate'),
            '4'   => __('4 Columns','crate'),
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_item_fancybox_control',array(
		'label' => __('Fancybox', 'crate'),
		'description' => __('Switch on the Gallery, the projects will be displayed in fancybox slider.','crate'),
		'section' => 'neuron_portfolio_item_options',
		'settings' => 'neuron_portfolio_item_fancybox',
		'type' => 'select',
		'choices' => array(
            '1'   => __('On','crate'),
            '2'   => __('Off','crate'),
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_item_navigation_control',array(
		'label' => __('Portfolio Item Navigation', 'crate'),
		'description' => __('Hide or show the navigation on portfolio single.','crate'),
		'section' => 'neuron_portfolio_item_options',
		'settings' => 'neuron_portfolio_item_navigation',
		'type' => 'select',
		'choices' => array(
            '1'   => __('Show','crate'),
            '2'   => __('Hide','crate'),
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_portfolio_item_navigation_control',array(
		'label' => __('Back to Portfolio', 'crate'),
		'description' => __('Pickup your page to get back to portfolio, if blank nothing will show up.','crate'),
		'section' => 'neuron_portfolio_item_options',
		'settings' => 'neuron_portfolio_item_backtoportfolio',
		'type' => 'dropdown-pages'
	)));
	// Blog Options
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_blog_style_control',array(
		'label' => __('Style', 'crate'),
		'description' => __('Choose your blog layout.','crate'),
		'section' => 'neuron_blog_options',
		'settings' => 'neuron_blog_style',
		'type' => 'select',
		'choices' => array(
            '1'   => __('Classic','crate'),
            '2'   => __('Grid','crate'),
        )
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_blog_columns_control',array(
		'label' => __('Columns', 'crate'),
		'description' => __('Please select the columns, it affects only in grid.','crate'),
		'section' => 'neuron_blog_options',
		'settings' => 'neuron_blog_columns',
		'type' => 'select',
		'choices' => array(
			'1'   => __('Full Width','crate'),
			'2'   => __('2 Columns','crate'),
			'3'   => __('3 Columns','crate'),
			'4'   => __('4 Columns','crate'),
		)
	)));
	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'neuron_blog_ppp_control',array(
		'label' => __('Posts Per Page', 'crate'),
		'description' => __('Write how many portfolio items you want to display, write -1 if you want to show all the posts.','crate'),
		'section' => 'neuron_blog_options',
		'settings' => 'neuron_blog_ppp',
		'type' => 'text',
	)));
}
add_action('customize_register', 'neuron_customize_options');

// Output Customizer Options
function neuron_customize_css(){ ?>
    <style type="text/css">
        body { background-color: <?php echo get_theme_mod('neuron_body_color'); ?> }
		header .nav-holder nav ul li > a { color: <?php echo get_theme_mod('neuron_header_color'); ?> }
		header .nav-holder .underline a:before { background-color: <?php echo get_theme_mod('neuron_header_color'); ?> }
		header .logo img { width: <?php echo get_theme_mod('neuron_header_logo_width'); ?>px }
		header .logo img { height: <?php echo get_theme_mod('neuron_header_logo_height'); ?>px }
		.container { background-color: <?php echo get_theme_mod('neuron_wrapper_color'); ?> }
    </style>

<?php }
add_action('wp_head','neuron_customize_css');


/**
 * Demo Importer
 * 
 * Import the content, widgets and
 * the customizer settings via the
 * plugin one click demo importer.
 */
add_filter('pt-ocdi/import_files', 'crate_ocdi_import_files');
function crate_ocdi_import_files() {
	return array(
		array(
			'import_file_name'           => esc_html__('Home', 'crate'),
			'local_import_file'            => get_template_directory() . '/inclucdes/demo-files/content.xml',
			'local_import_customizer_file' => get_template_directory() . '/inclucdes/demo-files/customizer.dat',
			'import_notice'              => esc_html__('Everything that is listed in our demo will be imported via this option.', 'crate'),
		),
	);
}

/**
 * After Import Setup
 * 
 * Set the Classic Home Page as front
 * page and assign the menu to 
 * the main menu location.
 */
add_action('pt-ocdi/after_import', 'crate_ocdi_after_import_setup');
function crate_ocdi_after_import_setup() {
	$main_menu = get_term_by('name', 'Main Menu', 'nav_menu');

	if ($main_menu) {
		set_theme_mod('nav_menu_locations', array('main-menu' => $main_menu->term_id));
	}

	$front_page_id = get_page_by_title('Home');
	if ($front_page_id) {
		update_option('page_on_front', $front_page_id->ID);
		update_option('show_on_front', 'page');
	}	
}