<?php
    /* Template Name: Portfolio */

    get_header();

    // Define variables
    $crate_portfolio_sortable = get_field("neuron_portfolio_sortable");
    $crate_portfolio_category = get_field("neuron_portfolio_category");

    global $post;
    $content = $post->post_content;
    if(!empty($content)) :
?>
    <div class="page-title">
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-lg-8 col-md-8 col-sm-12">
                <?php echo wpautop($content); ?>
            </div>
        </div>
    </div>
<?php
    endif;

    if($crate_portfolio_category){
        $crate_portfolio_cat = $crate_portfolio_category;
    }
    else {
        $crate_portfolio_array = array();

        $terms = get_terms( 'portfolio_category', array(
            'hide_empty' => false,
            'orderby' => 'term_id'
        ));

        foreach($terms as $term){
            $crate_portfolio_array[] .= $term->term_id;
        }
        $crate_portfolio_cat = $crate_portfolio_array;
    }

    if($crate_portfolio_sortable && $crate_portfolio_category) :
?>
<div class="filters">
    <ul id="filters">
        <li class="active" data-filter="*"><?php echo esc_attr__("All", "crate"); ?></li>
        <?php
            foreach($crate_portfolio_cat as $filter) :
                $term = get_term_by('id', $filter, 'portfolio_category');
        ?>
            <li data-filter=".<?php echo esc_attr($term->slug); ?>"><?php echo esc_attr($term->name); ?></li>
        <?php endforeach; ?>
    </ul>
</div>
<?php
    endif;

    // Portfolio Masonry
    $crate_portfolio_masonry = get_field("neuron_portfolio_masonry");
    if($crate_portfolio_masonry){
        $crate_portfolio_row = "portfolio masonry row";
    }
    else {
        $crate_portfolio_row = "portfolio masonry portfolio-fit-rows row";
    }

    // Portfolio Style
    $crate_portfolio_style = get_field("neuron_portfolio_style");
    if(!$crate_portfolio_style){
        $crate_portfolio_style = 1;
    }
    if($crate_portfolio_style == '1'){
        $crate_portfolio_style = get_theme_mod("neuron_portfolio_style");
        $crate_portfolio_style_mod = $crate_portfolio_style;
    }
    else {
        $crate_portfolio_style_mod = $crate_portfolio_style - 1;
    }

    if($crate_portfolio_style_mod == '2' || $crate_portfolio_style_mod == 2){
        $crate_portfolio_row .= " alternative";
    }

    // Portfolio Gallery
    $crate_portfolio_gallery = get_field("neuron_portfolio_gallery");
    $crate_portfolio_gallery_mod = get_theme_mod("neuron_portfolio_fancybox");
    if($crate_portfolio_gallery || $crate_portfolio_gallery_mod == '1'){
        $crate_portfolio_row .= " gallery-active";
    }
?>
<div class="portfolio-holder">
    <div class="<?php echo esc_attr($crate_portfolio_row); ?>">
        <?php
            // Portfolio variables
            $crate_item_selector = "col-lg-4 col-md-4 col-sm-6 col-xs-12 selector";

            // Portfolio Columns
            $crate_portfolio_columns = get_field("neuron_portfolio_columns");
            if(!$crate_portfolio_columns){
				$crate_portfolio_columns = 1;
			}
            if($crate_portfolio_columns == '1'){
				$crate_portfolio_columns = get_theme_mod("neuron_portfolio_columns");
				$crate_portfolio_columns_mod = $crate_portfolio_columns;
			}
			else {
				$crate_portfolio_columns_mod = $crate_portfolio_columns - 1;
			}
            switch($crate_portfolio_columns_mod){
                case '1':
                case 1:
                    $crate_item_selector = "col-lg-12 selector";
                    break;
                case '2':
                case 2:
                    $crate_item_selector = "col-lg-6 col-md-6 col-sm-6 col-xs-12 selector";
                    break;
                case '3':
                case 3:
                    $crate_item_selector = "col-lg-4 col-md-4 col-sm-6 col-xs-12 selector";
                    break;
                case '4':
                case 4:
                    $crate_item_selector = "col-lg-3 col-md-3 col-sm-6 col-xs-12 selector";
                    break;
            }

            // Posts per Page
            $crate_portfolio_ppp_mod = get_theme_mod("neuron_portfolio_ppp");
            $crate_portfolio_posts_per_page = get_field("neuron_portfolio_ppp");
            if($crate_portfolio_posts_per_page){
                $crate_portfolio_ppp = $crate_portfolio_posts_per_page;
            }
            else {
                if($crate_portfolio_ppp_mod){
                    $crate_portfolio_ppp = $crate_portfolio_ppp_mod;
                }
                else {
                    $crate_portfolio_ppp = 9;
                }
            }

            // Portfolio Category
            if($crate_portfolio_category){
                $crate_portfolio_cat = "IN";
            }
            else {
                $crate_portfolio_cat = "NOT IN";
            }

            // Paged
            if(get_query_var('paged')){
                $paged = get_query_var('paged');
            }
            elseif(get_query_var('page')){
                $paged = get_query_var('page');
            }
            else {
                $paged = 1;
            }

            $args = array(
                'post_type' => 'portfolio',
                'posts_per_page' => $crate_portfolio_ppp,
                'paged' => $paged,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'portfolio_category',
                        'field' => 'term_id',
                        'terms' => $crate_portfolio_category,
                        'operator' => $crate_portfolio_cat,
                    )
                ),
            );
            $query = new WP_Query($args);

            if($query->have_posts()) : while($query->have_posts()) : $query->the_post();

            $terms = get_the_terms( get_the_ID(), 'portfolio_category');
        ?>
            <div class="<?php echo esc_attr($crate_item_selector) ?> <?php if($terms){foreach($terms as $cat){ echo esc_attr($cat->slug) ." ";}} ?>">
                <?php
                    $crate_portfolio_post_url = get_permalink();
                    $crate_portfolio_title = "";
                    if($crate_portfolio_gallery || $crate_portfolio_gallery_mod == '1'){
                        $crate_portfolio_post_url = get_the_post_thumbnail_url();
                        $crate_portfolio_title = get_the_title();
                    }
                ?>
                <a href="<?php echo esc_attr($crate_portfolio_post_url); ?>" title="<?php echo esc_attr($crate_portfolio_title) ?>">
                    <div class="item-holder">
                        <div class="item">
                            <div class="overlay-background"></div>
                            <div class="overlay">
                                <div class="inner-overlay">
                                    <h3><?php the_title(); ?></h3>
                                    <?php
                                        $term_args = array(
                                            'orderby' => 'term_id'
                                        );
                                        $terms = wp_get_object_terms( get_the_ID(), 'portfolio_category', $term_args );
                                    ?>
                                    <span><?php foreach($terms as $cat){ echo esc_attr($cat->name) . " "; } ?></span>
                                </div>
                            </div>
                        </div>
                        <?php
                            // Thumbnail
                            if(has_post_thumbnail()){
                                the_post_thumbnail();
                            }
                            else {
                                $crate_count = count(get_the_category());
                                if($crate_count >= 10){
                                    echo '<img src="' . get_template_directory_uri() . '/assets/images/default-tall.png" />';
                                }
                                else {
                                    echo '<img src="' . get_template_directory_uri() . '/assets/images/default.png" />';
                                }
                            }
                        ?>
                    </div>
                </a>
            </div>
        <?php endwhile; endif; wp_reset_postdata(); ?>
    </div>
    <?php neuron_pagination($query->max_num_pages, "neuron-pagination", 999); ?>
</div>
<?php
    get_footer();
?>
