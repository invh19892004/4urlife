<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Document Settings -->
	<meta charset="<?php bloginfo('charset'); ?>">
	<?php if(get_theme_mod("neuron_responsivity") != 'disable') : ?>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php endif; ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="container">
		<header>
			<div class="logo">
				<a href="<?php echo esc_url(home_url('/')); ?>">
                    <?php
                        $crate_header_logo = get_theme_mod('neuron_header_logo');
                        $crate_header_logo_text = get_theme_mod('neuron_header_logo_text');
                        if($crate_header_logo){
                            echo "<img src=". esc_url($crate_header_logo) .">";
                        }
                        else if($crate_header_logo_text) {
                            echo esc_attr($crate_header_logo_text);
                        }
                        else {
                            echo esc_attr(bloginfo('name'));
                        }
                    ?>
				</a>
			</div>
			<div class="nav-holder">
				<div class="hamburger">
					<a href="#"><div class="hamburger-inner"></div></a>
				</div>
                <?php
                    $args = array(
                        'theme_location' => 'main-menu',
                        'container' => 'nav',
                        'menu_class' => 'underline'
                    );

                    if(has_nav_menu('main-menu')) {
                        wp_nav_menu($args);
                    }
                    else {
                        echo "<nav><ul class='underline'><li><a href='wp-admin/nav-menus.php'>" . esc_attr__("No menu assigned!","crate") ."</a></li</ul></nav>";
                    }
                ?>
			</div>
		</header>
