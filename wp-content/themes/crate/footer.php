        <?php
            $crate_footer_alignment = get_theme_mod("neuron_footer_alignment");
            $crate_footer_alignment_style = "text-align: center;";
            switch($crate_footer_alignment){
                case 'left':
                    $crate_footer_alignment_style = "text-align: left;";
                    break;
                case 'right':
                    $crate_footer_alignment_style = "text-align: right;";
                    break;
            }
            $crate_footer_copyright = get_theme_mod('neuron_footer_copyright', __('Copyright 2019. All Rights Reserved.', 'crate'));
            if($crate_footer_copyright) :
        ?>
        <footer style="<?php echo esc_attr($crate_footer_alignment_style); ?>" class="underline">
            <?php
                if($crate_footer_copyright){
                    echo wpautop($crate_footer_copyright);
                }
            ?>
        </footer>
        <?php endif; ?>
    </div>
    <?php wp_footer(); ?>
    </body>
</html>
