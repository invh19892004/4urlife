jQuery(document).ready(function($){
	"use strict";

	$(".hamburger").on("click", function(){
		$(this).toggleClass("is-active");
		$("header .nav-holder nav > ul").toggleClass("menu-active");
	});

	$('.portfolio.gallery-active').magnificPopup({
	  gallery: {
		enabled: true
	  },
	  image: {
		titleSrc: 'title'
	  },
	  delegate: '.selector > a',
	  type: 'image'
	});

	document.addEventListener("touchstart", function() {}, !0);

	$(window).load(function(){
		"use strict";
		var $container = $('.portfolio-holder .portfolio-fit-rows').isotope({
			layoutMode: "fitRows"
		});
		$(function() {
            // init Isotope
            var $container = $('.portfolio-holder .portfolio').isotope({
                itemSelector: '.selector'
            });
            // filter functions
            var filterFns = {
                // show if number is greater than 50
                numberGreaterThan50: function() {
                    var number = $(this).find('.number').text();
                    return parseInt(number, 10) > 50;
                },
                // show if name ends with -ium
                ium: function() {
                    var name = $(this).find('.name').text();
                    return name.match(/ium$/);
                }
            };
            // bind filter button click
            $('#filters').on('click', 'li', function() {
                var filterValue = $(this).attr('data-filter');
                // use filterFn if matches value
                filterValue = filterFns[filterValue] || filterValue;
                $container.isotope({
                    filter: filterValue
                });
            });
            // change is-checked class on buttons
            $('.filters').each(function(i, buttonGroup) {
                var $buttonGroup = $(buttonGroup);
                $buttonGroup.on('click', 'li', function() {
                    $buttonGroup.find('.active').removeClass('active');
                    $(this).addClass('active');
                });
            });
        });
	});
});
