<?php
    /* Template Name: About */
    get_header();

    if(have_posts()) : while(have_posts()) : the_post();
?>
<div class="about">
    <?php
        if(has_post_thumbnail()){
            the_post_thumbnail();
        }
    ?>
    <div class="about-content">
        <h5><?php the_title(); ?></h5>
        <?php the_content(); ?>
        <?php if(have_rows("neuron_about_social_icons")) : ?>
            <ul class="social-icons">
                <?php while(have_rows("neuron_about_social_icons")) : the_row(); ?>
                    <li>
                        <a target="_BLANK" href="<?php echo esc_attr(get_sub_field("neuron_about_social_icons_url")) ?>">
                            <i class="fa <?php echo esc_attr(get_sub_field("neuron_about_social_icons_icon")) ?>"></i>
                        </a>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>
<?php
    endwhile; endif;

    get_footer();
?>
