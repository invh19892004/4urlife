<?php
    get_header();
?>
<div class="blog">
    <div class="row">
        <div class="page-title col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-lg-8 col-md-8 col-sm-12">
            <p><?php echo esc_attr__("Search results for:", "crate") ?> <b><?php echo esc_attr(get_search_query()); ?></b></p>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <?php
                $args = array_merge( $wp_query->query_vars, array(
                    'post_type' => 'post'
                ));

                $query = new WP_Query($args);

                if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
            ?>
                <div id="id-<?php echo esc_attr(get_the_id()); ?>" <?php post_class("post"); ?>>
                    <div class="post-thumbnail">
                        <a href="<?php the_permalink(); ?>">
                            <?php
                                if(has_post_thumbnail()){
                                    the_post_thumbnail();
                                }
                            ?>
                        </a>
                    </div>
                    <div class="post-meta">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <ul class="underline">
                            <li class="date"><?php the_time('F j, Y'); ?></li>
                            <li class="category"><?php the_category(' '); ?></li>
                        </ul>
                    </div>
                    <div class="post-info">
                        <?php the_excerpt(); ?>
                    </div>
                    <div class="button-holder">
                        <a href="<?php the_permalink(); ?>" class="button"><?php echo esc_attr__("Read More", "crate"); ?></a>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); else : ?>
                <div class="nothing-found">
                    <h5><?php echo esc_attr__("Nothing Found","crate") ?></h5>
                    <p><?php echo esc_attr__("Sorry, but nothing matched your search terms. Please try again with some different keywords.","crate") ?></p>
                    <form role="search" action="<?php echo esc_url(home_url("/")) ?>" method="get">
                        <input type="search" placeholder="<?php echo esc_attr__("Search","crate") ?>" name="s" value="">
                    </form>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php
    get_footer();
?>
