<?php
    get_header();
?>
<div class="blog">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12">
            <?php
                $args = array_merge( $wp_query->query_vars, array(
                    'post_type' => array('post','portfolio'),
                ));
                $query = new WP_Query($args);

                if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
            ?>
                <div id="id-<?php echo esc_attr(get_the_id()); ?>" <?php post_class("post"); ?>>
                    <div class="post-thumbnail">
                        <a href="<?php the_permalink(); ?>">
                            <?php
                                if(has_post_thumbnail()){
                                    the_post_thumbnail();
                                }
                            ?>
                        </a>
                    </div>
                    <div class="post-meta">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <ul class="underline">
                            <li class="date"><?php the_time('F j, Y'); ?></li>
                            <li class="category"><?php the_category(' '); ?></li>
                        </ul>
                    </div>
                    <div class="post-info">
                        <?php the_excerpt(); ?>
                    </div>
                    <div class="button-holder">
                        <a href="<?php the_permalink(); ?>" class="button"><?php echo esc_attr__("Read More", "crate"); ?></a>
                    </div>
                </div>
            <?php endwhile; endif; wp_reset_postdata(); ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php
    get_footer();
?>
