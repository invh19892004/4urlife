<?php
    get_header();

    if(have_posts()) : while(have_posts()) : the_post();
?>
<div class="blog">
    <div class="row">
        <div class="col-lg-offset-1 col-md-offset-1 col-lg-10 col-md-10 col-sm-12">
            <div class="post">
                <div class="post-thumbnail">
                    <?php
                        if(has_post_thumbnail()){
                            the_post_thumbnail();
                        }
                    ?>
                </div>
                <div class="post-meta">
                    <h3><?php the_title(); ?></h3>
                    <ul class="underline">
                        <li class="date"><?php the_time('F j, Y'); ?></li>
                        <li class="category"><?php the_category(' '); ?></li>
                    </ul>
                </div>
                <div class="post-info">
                    <?php
                        the_content();
                        wp_link_pages();
                    ?>
                </div>
                <div class="post-tags">
                    <h6><?php echo esc_attr__("Tags:", "crate"); ?></h6>
                    <?php the_tags('<ul><li>', '</li><li>', '</li></ul>'); ?>
                </div>
            </div>
        	<?php comments_template(); ?>
        </div>
    </div>
</div>
<?php
    endwhile; endif;

    get_footer();
?>
