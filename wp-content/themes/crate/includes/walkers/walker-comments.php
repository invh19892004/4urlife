<?php
	class comment_walker extends Walker_Comment {

		var $tree_type = 'comment';
		var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );

		// constructor – wrapper for the comments list

		function __construct() { ?>

			<ul class="comment-list">

		<?php }

		// start_lvl – wrapper for child comments list

		function start_lvl( &$output, $depth = 0, $args = array() ) {

			$GLOBALS['comment_depth'] = $depth + 2; ?>

			<section class="child-comments comments-list">

		<?php }

		// end_lvl – closing wrapper for child comments list

		function end_lvl( &$output, $depth = 0, $args = array() ) {
			$GLOBALS['comment_depth'] = $depth + 2; ?>

			</section>
		<?php }
		// start_el – HTML for comment template

		function start_el( &$output, $comment, $depth = 0, $args = Array(), $id = 0 ) {

			$depth++;
			$GLOBALS['comment_depth'] = $depth;
			$GLOBALS['comment'] = $comment;
			$parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' );

			if ( 'article' == $args['style'] ) {
				$tag = 'article';
				$add_below = 'comment';
			}
			else {
				$tag = 'article';
				$add_below = 'comment';
			}
		?>
			<li class="comments comment-line" <?php comment_class("single-comment clearfix", empty( $args['has_children'] ) ? '' :'parent') ?> id="comment-<?php comment_ID() ?>" itemscope itemtype="http://schema.org/Comment">
				<div class="comment">
					<div class="pic">
						<?php echo get_avatar( $comment, 65 ); ?>
					</div>
					<div class="comment-content">
						<h6 class="standard-title"><?php comment_author(); ?></h6>
						<span><?php the_time('F j, Y'); ?></span>
						<p><?php comment_text() ?></p>
					</div>
				</div>
			<?php }
			// end_el – closing HTML for comment template
			function end_el(&$output, $comment, $depth = 0, $args = array() ) { ?>
			</li>
		<?php }
		// destructor – closing wrapper for the comments list
		function __destruct() { ?>
			</ul>
		<?php }
	}
?>
