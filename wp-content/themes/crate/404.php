<?php
    get_header();
?>
<div class="row">
    <div class="error-404 col-lg-offset-4 col-md-offset-4 col-sm-offset-3 col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <h1><?php echo esc_attr__("404", "crate") ?></h1>
        <h3><?php echo esc_attr__("Page not found", "crate") ?></h3>
        <p><?php echo esc_attr__("The page requested couldn't be found. This could be a spelling error in the URL or a removed page.", "crate") ?></p>
    </div>
</div>
<?php
    get_footer();
?>
